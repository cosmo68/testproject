/**
  ******************************************************************************
  * @file    TestProject/main.c
  * @author  Alexandr Shipovsky
  * @version V0.0.1
  * @date    22-May-2019
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * 
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stm8l15x.h"
#include "stm8l15x_clk.h"
#include "stm8l15x_gpio.h"
#include "stm8l15x_syscfg.h"
#include "stm8l15x_usart.h"
#include "delay/delay.h"
#include "LIS302DL.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define UART_TX GPIO_Pin_5
#define UART_RX GPIO_Pin_6
#define BAUD 9600
#define del 4
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void CLK_Cfg(void);       // ��������� ������������
void HM10_Cfg(void);      // ��������� BLE ������ HM-10
void UART_Cfg(void);      // ��������� UART
void Writeln_UART(USART_TypeDef *  USARTx,char * string);  // ���������� ������ � UART

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  CLK config
  * @param  None
  * @retval None
  */
void CLK_Cfg(void)
{
  CLK_DeInit();          // ����� �������� ������������
  
  CLK_SYSCLKSourceSwitchCmd(ENABLE);
  CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSE); // �������� ������� ����� � �������� ��������� ������������  
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_2);   // ������������� ������������ ������� (8/2 = 4���)

  while (CLK_GetSYSCLKSource() != CLK_SYSCLKSource_HSE){};  // ���� ���� ������� ����� �� ���� ���������� ������������  
  CLK_HSICmd(DISABLE);          // ��������� ���������� ���������
}  
/**
  * @brief  UART config
  * @param  None
  * @retval None
  */
void UART_Cfg(void)
{
  SYSCFG_REMAPPinConfig(REMAP_Pin_USART1TxRxPortC,ENABLE);     // ������������� ���� UART �� ���� C
  
  GPIO_ExternalPullUpConfig(GPIOC, UART_TX | UART_RX, ENABLE); // ����������� PC5 � PC6 �� �������������� �������
  
  CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);    // �������� ������������ UART
  /* USART configured as follow:
          - BaudRate = BAUD  
          - Word Length = 8 Bits
          - One Stop Bit
          - NO parity
          - Receive and transmit enabled        
  */
  USART_Init(USART1,BAUD,
             USART_WordLength_8b,
             USART_StopBits_1,
             USART_Parity_No,
             (USART_Mode_TypeDef)(USART_Mode_Tx | USART_Mode_Rx));
}
/**
  * @brief  String to UART
  * @param  USART_TypeDef *  USARTx,
  *         char * string
  * @retval None
  */
void Writeln_UART(USART_TypeDef *  USARTx,char * string)
{
	uint8_t i;
	for(i = 0;string[i];i++)
        {
	USART_SendData8(USARTx,string[i]);                           // ��������� i-� ���� ������
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);  // ���� ����� ��������� ��������
	} // FOR
        USART_SendData8(USARTx,'\0');   // ����� ������
        USART_SendData8(USARTx,'\n');   // ������� ������
        USART_SendData8(USARTx,'\r');   // ������� �������
}
/**
  * @brief  Configurated HM10
  * @param  None
  * @retval None
  */
void HM10_Cfg(void)
{
  Writeln_UART(USART1,"AT+ADVI5");       // ��������� ��������� �������
  delay_ms(del);
  Writeln_UART(USART1,"AT+NAMETESTPRJ"); // ��������� �����
  delay_ms(del);
  Writeln_UART(USART1,"AT+ADTY3");       // ����� ������ ����������������� �������
  delay_ms(del);
  Writeln_UART(USART1,"AT+IBEA1");       // ��������� ������ �����(IBEACON)
  delay_ms(del);
  Writeln_UART(USART1,"AT+DELO");        // ������ ������ � ������ ��������
  delay_ms(del);
}  
/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
void main(void)
{
  CLK_Cfg();
  UART_Cfg();
  HM10_Cfg();
  
  LIS302DL_InitTypeDef LIS302DL;         // ��������� � ��������� ��������� � ����������� �������������
  LIS302DL.SampleFreq = SampleFreq_100;
  LIS302DL.Power = Power_ENABLE;
  LIS302DL.Earth = EARTH_ENABLE;
  LIS302DL.Range = Range_2G;
  LIS302DL.Filter = Filter_MODE00;
  char *buf;                             // ��������� �� ������, ������������ UART��
  _Bool ErStatus;
  ErStatus = LIS302DL_Cfg(&LIS302DL);    // ������������� LIS302DL
  LIS302DL_OutTypeDef LIS302DL_OUT;
  /* Infinite loop */
  while (1)
  {
   if(ErStatus)  // ���� ����������� �� �����������, ���������� � UART 
   {
     buf = "Error, LIS302DL not connected";
   }
   else
   {
     LI302DL_GetData(&LIS302DL_OUT); // ��������� ������
     printf(buf,"X: %d\n\rY: %d\n\rZ: %d\n\r",LIS302DL_OUT.X,LIS302DL_OUT.Y,LIS302DL_OUT.Z); // ��������������� �����
   }
   delay_ms(100);
   Writeln_UART(USART1,buf); // ���������� � UART
  }
  /* END WHILE */
}
/* END MAIN */
#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

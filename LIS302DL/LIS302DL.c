/**
  ******************************************************************************
  * @file    LIS302DL/LIS302DL.c
  * @author  Alexandr Shipovsky
  * @version V0.0.1
  * @date    22-May-2019
  * @brief   Main program body
  ******************************************************************************
  * @attention
  * ���������� ��� ������ � 3-� ������ �������������� ����� SPI1.
  *
  * 
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "LIS302DL.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
void SPI1_Cfg(void);                  // ������������� SPI1
uint8_t WriteSPI1(uint8_t data);      // ���������� ���� � SPI1 � ���������� �������� ����
void SetReg(uint8_t val,uint8_t adr); // ���������� val � ������� � ������� adr
uint8_t GetReg(uint8_t adr);          // ���������� �������� �������� � ������� adr

/**
  * @brief  ������������� �������������. ���������� 0 ���� ����� �� �����������
  * @param  None
  * @retval None
  */
_Bool LIS302DL_Cfg(LIS302DL_InitTypeDef *LIS302DL_InitStructure)
{
  uint8_t BufReg = 0x00;
  SPI1_Cfg();
  if(GetReg(LIS302DL_WHO_AM_I)==0x3B)
  {
  BufReg|=LIS302DL_InitStructure->Range|LIS302DL_InitStructure->Power|LIS302DL_InitStructure->SampleFreq; // ���������� ��������� � ���� ����
  SetReg(BufReg,LIS302DL_CTRL_REG1); //����������� ������� CTRL_REG1
  BufReg|=LIS302DL_InitStructure->Earth|LIS302DL_InitStructure->Filter;
  SetReg(BufReg,LIS302DL_CTRL_REG2); //����������� ������� CTRL_REG2
  return 1;
  }
  else return 0;
}
/**
  * @brief  ��������� ��������� LIS302DL_OUT ���������� �������
  * @param  None
  * @retval None
  */
void LI302DL_GetData(LIS302DL_OutTypeDef *LIS302DL_OUT)
{
  LIS302DL_OUT->X = (int8_t)GetReg(LIS302DL_OUT_X); // ��������� �������� ��� X
  LIS302DL_OUT->Y = (int8_t)GetReg(LIS302DL_OUT_Y); // ��������� �������� ��� Y
  LIS302DL_OUT->Z = (int8_t)GetReg(LIS302DL_OUT_Z); // ��������� �������� ��� Z
}
/* Private functions ---------------------------------------------------------*/
/**
  * @brief  SPI1_Cfg
  * @param  None
  * @retval None
  */
void SPI1_Cfg(void)
{
  CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);  // �������� ������������ SPI

  GPIO_ExternalPullUpConfig(GPIOB,GPIO_Pin_4 | GPIO_Pin_5 |GPIO_Pin_6 | GPIO_Pin_7, ENABLE);  // ����������� ���� �� �������������� �������

  SPI_Init(SPI1, SPI_FirstBit_MSB, SPI_BaudRatePrescaler_2, SPI_Mode_Master,                  // �������������� SPI1 ��� ������ � ������ ������� �������� 
           SPI_CPOL_Low, SPI_CPHA_1Edge, SPI_Direction_2Lines_FullDuplex,
           SPI_NSS_Hard, 0x07);

  SPI_Cmd(SPI1, ENABLE); // �������� SPI1

}
/**
  * @brief  ���������� ���� data � SPI1 � ���������� �������� ����
  * @param  None
  * @retval None
  */
uint8_t WriteSPI1(uint8_t data)
{
  while(SPI_GetFlagStatus(SPI1,SPI_FLAG_BSY) == SET); // ���� ���� ����������� SPI1
  SPI_SendData(SPI1,data);     // ���������� ����
  while(SPI_GetFlagStatus(SPI1,SPI_FLAG_BSY) == SET); // ���� ���� ����������� SPI1
  return SPI_ReceiveData(SPI1); // ������ ������
}
/**
  * @brief  ���������� val � ������� � ������� adr
  * @param  None
  * @retval None
  */
void SetReg(uint8_t val,uint8_t adr)
{
  WriteSPI1(adr); // ���������� ����� ��������
  WriteSPI1(val); // ���������� ��������
}
/**
  * @brief  ���������� �������� �������� � ������� adr
  * @param  None
  * @retval None
  */
uint8_t GetReg(uint8_t adr)
{
  uint8_t data = 0;
  adr|=(1<<7);    // ������������� ������ ��� � ������� ��� ������ �������� � ������� adr
  WriteSPI1(adr); // ���������� ����� ��������
  data = WriteSPI1(0x00); // ���������� ������ ��������, ��� ��� ������������ �� ������� ���� ����� ������ �� ��������� ���� ������
  return data;    // ���������� ��������� ����
}